package com.sg01.service;

import com.tmax.proobject.engine.service.executor.ServiceExecutor;

public class UserCreateServiceExecutor extends ServiceExecutor {

	public UserCreateServiceExecutor() {
		serviceObject = new UserCreateService();
	}
	
	@Override
	public Object execute(Object input, String methodName) throws Throwable {
		return serviceObject.service(input);
	}

}
